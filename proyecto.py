from flask import Flask, render_template, url_for, request, redirect, session
from datos.usuario import Usuario
from datos.receta import Receta
from datos.autor import Autor
from datos.comentario import Comentario
from datos.registro import Registro
from datos.registroadmin import RegistroAdmin
from datos.usuarioadmin import UsuarioAdmin
import base64
import csv
from os import environ
import datetime
#from tkinter import messagebox as MessageBox 
import json
app = Flask(__name__)

registrados = []
registradosadmin = []
usuarios = []
usuariosadmin = []
recetas = []
comentarios = []
autores = []
autorP = Autor("PIZZA DELUXE", "Jose Ceron")
autores.append(autorP)

recetaejemplo = Receta("PIZZA DELUXE","Disfrutar esta pizza es todo un lujo. Pepperoni, carne molida, champiñones, pimiento, cebolla.","Salsea las bases y esparce sobre ella el jamón y la piña. Espolvorea el queso y hornea por 20 minutos o hasta que la masa esté cocida Revuelve el chile habanero en un tazón junto con la cebolla morada y el aceite de oliva. Cuando esté lista la pizza, cucharea esta mezcla sobre ella y lleva a la mesa.","Pepperoni, Carne Molida, Champiñones Frescos, Pimiento y Cebolla","30 minutos","https://static.wixstatic.com/media/0a989b_c898401c324347f3a7ddfbdd5d777da4.jpg/v1/fill/w_441,h_294,al_c,lg_1,q_85/0a989b_c898401c324347f3a7ddfbdd5d777da4.webp",None)
recetas.append(recetaejemplo)
registraradmin = RegistroAdmin("Usuario","Maestro","admin","admin","admin")
registradosadmin.append(registraradmin)

usuarioadm = UsuarioAdmin("admin","admin")
usuariosadmin.append(usuarioadm)

app.secret_key = b'hola123'

@app.route('/')
def index():
    url_for('static',filename='w3.css')
    url_for('static',filename='translateelement.css')
    if 'usuario_logeado' in session:
        return render_template('index.html', usuario=session['usuario_logeado'], recetas = recetas, usuarioadmin=session['usuario_logeado'],comentarios = comentarios, autores = autores)
    return render_template('index.html', usuarioadmin=None ,usuario=None, recetas = recetas, comentarios = comentarios, autores = autores)

#@app.route('/login')
#def login():
#    return render_template('login.html')

#@app.route('/registro')
#def registro():
#    return render_template('registro.html')
#app.run(debug=True) 

def validar_campos(nom, ap, us, cont, confcont):
    if nom == "" or ap =="" or us =="" or cont =="" or confcont =="":
        return None
    else:
        return registro
  
def validar_cont(contr, confcont):
    if contr == confcont:
        return 'a'
    else:
        return 'b'   

def validar_login(user, contrasena):
    for usuario in usuarios:
        if usuario.nombre == user and usuario.contrasena == contrasena:
            return usuario
    return None

def validar_loginadmin(user, contrasena):
    for usuarioadmin in usuariosadmin:
        if usuarioadmin.nombre == user and usuarioadmin.contrasena == contrasena:
            return usuarioadmin
    return None

def modificar_perfil(user):
    for usuario in usuarios:
        if usuario.nombre != None:
            usuario.nombre = 'x' 
    return None

def obtener_contrasena(user):
    for usuario in usuarios:
        if usuario.nombre == user:
            return usuario.contrasena
    return None

def usuario_no_repetido(user):
    for registro in registrados:
        if registro.usuario == user:
            return 'x'
    return 'r'

def usuario_no_repetidoadmin(user):
    for registroadmin in registradosadmin:
        if registroadmin.usuario == user:
            return 'xx'
    return 'rr'

def busqueda(user):
    for registroadmin in registradosadmin:
        for usuario in usuarios:
            if registroadmin.usuario == usuario.nombre:
                return user
    return 'r'

def busquedacont(user):
    for registroadmin in registradosadmin:
        for usuario in usuarios:
            if registroadmin.usuario == usuario.nombre:
                return user
    return 'r'


@app.route('/registro', methods=['POST', 'GET'])
def registro(): 
    error = None
    cont = ''
    repetido = ''
    if request.method == 'POST':
        registro = validar_campos(request.form['nombre'], request.form['apellido'],request.form['usuario'],request.form['contrasena'], request.form['confcontrasena']) 
        if registro != None: 
            contras = validar_cont(request.form['contrasena'], request.form['confcontrasena'])
            if contras == 'a':
                repetido = usuario_no_repetido(request.form['usuario'])
                repetidoadmin = usuario_no_repetidoadmin(request.form['usuario'])
                if repetido == 'r' and repetidoadmin =='rr':  
                    registro1 = Registro(request.form['nombre'], request.form['apellido'],request.form['usuario'],request.form['contrasena'], request.form['confcontrasena'])
                    usuario1 = Usuario(request.form['usuario'],request.form['contrasena'])
                    registrados.append(registro1)
                    usuarios.append(usuario1) 
                    return redirect('/')
                else:
                    repetido = 'Nombre de Usuario registrado'
                    return render_template('registro.html', repetido=repetido)
            else: 
                cont = 'Las contraseñas no coinciden'
                return render_template('registro.html', cont=cont)
        else:
            error = 'Llene todos los campos'
            return render_template('registro.html', error=error) 
    url_for('static',filename='w3.css')
    url_for('static',filename='translateelement.css')
    return render_template('registro.html', error=error)

@app.route('/registroadmin', methods=['POST', 'GET'])
def registroadmin(): 
    error = None
    cont = ''
    repetido = ''
    if request.method == 'POST':
        registroadmin = validar_campos(request.form['nombre'], request.form['apellido'],request.form['usuario'],request.form['contrasena'], request.form['confcontrasena']) 
        if registroadmin != None: 
            contras = validar_cont(request.form['contrasena'], request.form['confcontrasena'])
            if contras == 'a':
                repetido = usuario_no_repetido(request.form['usuario'])
                repetidoadmin = usuario_no_repetidoadmin(request.form['usuario'])
                if repetido == 'r' and repetidoadmin == 'rr':  
                    registroad = RegistroAdmin(request.form['nombre'], request.form['apellido'],request.form['usuario'],request.form['contrasena'], request.form['confcontrasena'])
                    usuario1 = UsuarioAdmin(request.form['usuario'],request.form['contrasena'])
                    registradosadmin.append(registroad)
                    usuariosadmin.append(usuario1)
                    return redirect('/menuadmin')
                else:
                    repetido = 'Nombre de Usuario registrado'
                    return render_template('registroadmin.html', repetido=repetido)
            else: 
                cont = 'Las contraseñas no coinciden'
                return render_template('registroadmin.html', cont=cont)
        else:
            error = 'Llene todos los campos'
            return render_template('registroadmin.html', error=error) 
    url_for('static',filename='w3.css')
    url_for('static',filename='translateelement.css')
    return render_template('registroadmin.html', error=error)

@app.route('/login', methods=['POST', 'GET'])
def login():
    error = None
    if request.method == 'POST':
        usuario = validar_login(request.form['usuario'], request.form['contrasena'])
        usuarioadmin = validar_loginadmin(request.form['usuario'], request.form['contrasena'])
        if usuario != None or usuarioadmin != None:
            for registroadmin in registradosadmin:
                if request.form['usuario'] == registroadmin.usuario and request.form['contrasena'] == registroadmin.contrasena:
                    session['usuario_logeado'] = usuarioadmin.nombre
                    return redirect('/menuadmin')
            session['usuario_logeado'] = usuario.nombre
            return redirect('/')
        else:
            error = 'Contrasena invalida'
            return render_template('login.html', error=error)
    if 'usuario_logeado' in session:
        return redirect('/')
    url_for('static',filename='w3.css')
    url_for('static',filename='translateelement.css')
    return render_template('login.html', error=error)

@app.route('/logout', methods=['GET'])
def logout():
    session.pop('usuario_logeado', None)
    return redirect('login')

@app.route('/modificar',methods=['POST', 'GET'])
def modificar():
    repetido = ''
    if request.method == 'POST':
        for usuario in usuarios:
            for registro in registrados:
                if 'usuario_logeado' in session:
                    if session['usuario_logeado'] == usuario.nombre:
                        if  usuario.nombre == registro.usuario:
                            usrepetido = usuario_no_repetido(request.form['modusuario'])
                            if  usrepetido == 'r':
                                if request.form['modnombre'] != '':
                                    x = request.form['modnombre']
                                    registro.nombre = x
                                if request.form['modapellido'] != '':
                                    y = request.form['modapellido']
                                    registro.apellido = y
                                if request.form['modusuario'] != '':
                                    z = request.form['modusuario']
                                    registro.usuario = z
                                    usuario.nombre = z
                                if request.form['modcontrasena'] != '':
                                    a = request.form['modcontrasena']
                                    registro.contrasena = a
                                    registro.confcontrasena = a
                                    usuario.contrasena = a
                                session.pop('usuario_logeado', None)
                                return redirect('/')
                            else:
                                repetido = 'Nombre de Usuario registrado'
                                return render_template('modificar.html', repetido=repetido)
    url_for('static',filename='w3.css')
    url_for('static',filename='translateelement.css')
    return render_template('modificar.html', repetido = repetido)

#hola

@app.route('/modificaradmin',methods=['POST', 'GET'])
def modificaradmin():
    repetido = ''
    if request.method == 'POST':
        for usuarioadmin in usuariosadmin:
            for registroadmin in registradosadmin:
                if 'usuario_logeado' in session:
                    if session['usuario_logeado'] == usuarioadmin.nombre:
                        if  usuarioadmin.nombre == registroadmin.usuario:
                            usrepetido = usuario_no_repetidoadmin(request.form['modusuario'])
                            if  usrepetido == 'rr':
                                if request.form['modnombre'] != '':
                                    x = request.form['modnombre']
                                    registroadmin.nombre = x
                                if request.form['modapellido'] != '':
                                    y = request.form['modapellido']
                                    registroadmin.apellido = y
                                if request.form['modusuario'] != '':
                                    z = request.form['modusuario']
                                    registroadmin.usuario = z
                                    usuarioadmin.nombre = z
                                if request.form['modcontrasena'] != '':
                                    a = request.form['modcontrasena']
                                    registroadmin.contrasena = a
                                    registroadmin.confcontrasena = a
                                    usuarioadmin.contrasena = a
                                session.pop('usuario_logeado', None)
                                return redirect('/')
                            else:
                                repetido = 'Nombre de Usuario registrado'
                                return render_template('modificaradmin.html', repetido=repetido)
    url_for('static',filename='w3.css')
    url_for('static',filename='translateelement.css')
    return render_template('modificaradmin.html', repetido = repetido)

@app.route('/recuperar', methods=['POST', 'GET'])
def recuperar():
    us = None
    error = None
    if request.method == 'POST':
        us = obtener_contrasena(request.form['obtusuario'])
        if us == None:
            error = 'Usuario no registrado'
    url_for('static',filename='w3.css') 
    url_for('static',filename='translateelement.css')
    return render_template('recuperar.html', error=error, us=us)

@app.route('/agregar_receta')
def agregar():
    return render_template('agregar_receta.html', recetas = recetas)

@app.route('/postReceta', methods=['POST'])
def agregar_receta():
    datos = request.get_json()
    if datos['titulo'] == '' or datos['resumen'] == '' or datos['ingredientes'] == '' or datos['procedimiento'] == '' or datos['tiempo_de_preparacion'] == '' or datos['imagen'] == '':
        return {"msg": 'Error en contenido'}
    receta = Receta(datos['titulo'], datos['resumen'], datos['ingredientes'], datos['procedimiento'], datos['tiempo_de_preparacion'], datos['imagen'],None)
    recetas.append(receta)
    ry = session['usuario_logeado'] 
    autorS = Autor(datos['titulo'],ry)
    autores.append(autorS)
    return {"msg": 'receta agregada'}
    #return MessageBox.showinfo("receta", "Receta agregada!")

@app.route('/postcomentario', methods=['POST']) 
def agregar_comentario():
    datos = request.get_json()
    if datos['comentario'] == '': 
        return {"msg": 'Error en contenido'}
    for receta in recetas:
        if datos['titulo'] == receta.titulo:
            receta.comentario = datos['comentario']
            x = session['usuario_logeado']
            ahora = datetime.datetime.now()
            comentario = Comentario(x,datos['titulo'],datos['comentario'],ahora.strftime('%d/%m/%Y %H:%M:%S'))
            comentarios.append(comentario)
            return {"msg": 'comentario agregado'} 
    return {"msg": 'Error en contenido'}

@app.route('/menuadmin')
def menuadmin():
    url_for('static',filename='w3.css') 
    url_for('static',filename='translateelement.css')
    if 'usuario_logeado' in session:
        return render_template('menuadmin.html', usuario=session['usuario_logeado'], recetas = recetas, registrados = registrados, registradosadmin = registradosadmin, comentarios = comentarios)
    return {"msg": 'Error en contenido'}

@app.route('/comentario')
def comentar():
    return render_template('comentario.html', recetas = recetas)

@app.route('/modificarReceta', methods=['POST', 'GET'])
def modificar_receta():
    error = None
    if request.method == 'POST':
        for receta in recetas:
            if receta.titulo == request.form['tituloamodificar']:
                if request.form['titulo'] != '':
                    receta.titulo = request.form['titulo']
                if request.form['resumen'] != '':
                    receta.resumen = request.form['resumen']
                if request.form['ingredientes'] != '':
                    receta.ingredientes = request.form['ingredientes']
                if request.form['procedimiento'] != '':
                    receta.procedimiento = request.form['procedimiento']
                if request.form['tiempo_de_preparacion'] != '':
                    receta.tiempo_de_preparacion = request.form['tiempo_de_preparacion']
                if request.form['imagen'] != '':
                    receta.imagen = request.form['imagen']
                receta.comentario = None
                return redirect('/menuadmin')
        error = 'Usuario no encontrado'
        return render_template('modificarReceta.html', error = error)
    url_for('static',filename='w3.css')
    url_for('static',filename='translateelement.css')
    return render_template('modificarReceta.html', error=error)

@app.route('/eliminarReceta', methods=['POST', 'GET'])
def eliminar_receta():
    error = None
    if request.method == 'POST':
        for receta in recetas:
            if receta.titulo == request.form['titulo']:
                receta.titulo = ""
                receta.resumen = ""
                receta.ingredientes = ""
                receta.procedimiento = ""
                receta.tiempo_de_preparacion = ""
                receta.imagen = ""
                receta.comentario = ""
                return redirect('/menuadmin')
        error = 'Usuario no encontrado'
        return render_template('eliminarReceta.html', error = error)
    url_for('static',filename='w3.css')
    url_for('static',filename='translateelement.css')
    return render_template('eliminarReceta.html', error=error)

@app.route('/cargarArchivo', methods=['POST'])
def agregarRecetas():
    datos = request.get_json()

    if datos['data'] == '':
        return {"msg": 'Error en contenido'}

    contenido = base64.b64decode(datos['data']).decode('utf-8')

    filas = contenido.splitlines()
    reader = csv.reader(filas, delimiter=',')
    for row in reader:
        receta = Receta(row[0], row[1], row[2], row[3], row[4], row[5], row[6])
        recetas.append(receta)
        ry = session['usuario_logeado']
        autorc = Autor(row[0],ry)
        autores.append(autorc)

    return {"msg": 'Receta agregada'}


if __name__ == '__main__':
    app.run(debug=True)