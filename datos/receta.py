class Receta:
    def __init__(self, titulo, resumen, ingredientes, procedimiento, tiempo_de_preparacion, imagen, comentario):
        self.titulo = titulo
        self.resumen = resumen
        self.ingredientes = ingredientes
        self.procedimiento = procedimiento
        self.tiempo_de_preparacion = tiempo_de_preparacion
        self.imagen = imagen
        self.comentario = comentario